-- MySQL dump 10.13  Distrib 5.6.21, for Win64 (x86_64)
--
-- Host: localhost    Database: sondaggio
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `idCategoria` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (0000000001,'Arte'),(0000000002,'Moto'),(0000000003,'Videogame'),(0000000004,'Film'),(0000000005,'Natura'),(0000000006,'Animali');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `composto`
--

DROP TABLE IF EXISTS `composto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `composto` (
  `fkIdSondaggio` int(10) unsigned zerofill NOT NULL,
  `fkIdDomanda` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`fkIdSondaggio`,`fkIdDomanda`),
  KEY `fkIdDomanda` (`fkIdDomanda`),
  CONSTRAINT `composto_ibfk_1` FOREIGN KEY (`fkIdDomanda`) REFERENCES `domanda` (`idDomanda`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `composto_ibfk_2` FOREIGN KEY (`fkIdSondaggio`) REFERENCES `sondaggio` (`idSondaggio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `composto`
--

LOCK TABLES `composto` WRITE;
/*!40000 ALTER TABLE `composto` DISABLE KEYS */;
INSERT INTO `composto` VALUES (0000000001,0000000001),(0000000003,0000000001),(0000000001,0000000002),(0000000003,0000000002),(0000000001,0000000003),(0000000003,0000000003),(0000000001,0000000004),(0000000003,0000000004),(0000000002,0000000005),(0000000002,0000000006),(0000000002,0000000007),(0000000004,0000000008),(0000000004,0000000009),(0000000004,0000000010),(0000000005,0000000011),(0000000005,0000000012),(0000000006,0000000013),(0000000006,0000000014),(0000000007,0000000015);
/*!40000 ALTER TABLE `composto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domanda`
--

DROP TABLE IF EXISTS `domanda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domanda` (
  `idDomanda` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `fkIdUser` int(10) unsigned zerofill NOT NULL,
  `domanda` varchar(45) NOT NULL,
  `r1` varchar(45) DEFAULT NULL,
  `r2` varchar(45) DEFAULT NULL,
  `r3` varchar(45) DEFAULT NULL,
  `r4` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idDomanda`),
  KEY `fkIdUser` (`fkIdUser`),
  CONSTRAINT `domanda_ibfk_1` FOREIGN KEY (`fkIdUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domanda`
--

LOCK TABLES `domanda` WRITE;
/*!40000 ALTER TABLE `domanda` DISABLE KEYS */;
INSERT INTO `domanda` VALUES (0000000001,0000000003,'Quale regista preferisci?','Quentin Tarantino','Woody Allen','Fratelli Coen','Steven Spielberg'),(0000000002,0000000003,'Ti piacciono i film drammatici?','','','',''),(0000000003,0000000003,'Ti piacciono i film comici?','','','',''),(0000000004,0000000003,'Che film preferisci?','Pulp Fiction','Il Sesto Senso','Il Miglio Verde','Fantozzi'),(0000000005,0000000003,'Ti piace la pittura?','','','',''),(0000000006,0000000003,'Che pittore preferisci?','Pablo Picasso','Giotto di Bondone','Leonardo Da Vinci','Paul C�zanne'),(0000000007,0000000003,'Quale quadro preferisci?','Monna Lisa','L\'urlo','Notte Stellata','L�ultima Cena'),(0000000008,0000000003,'Quale � il tuo gioco preferito?','Resident Evil','League of Legends','World of Warcraft','Starcraft'),(0000000009,0000000003,'Che consolle possiedi?','Play Station','XBox','Nintendo','Nessuna'),(0000000010,0000000003,'Hai un PC da gioco?','','','',''),(0000000011,0000000003,'Ti piacciono i cani?','','','',''),(0000000012,0000000003,'Ti piacciono i gatti?','','','',''),(0000000013,0000000003,'Hai una moto?','','','',''),(0000000014,0000000003,'Che moto ti piacciono?','Da strada','Da corsa','Da cross','Da enuro'),(0000000015,0000000003,'Ti piacciono i fiori?','','','','');
/*!40000 ALTER TABLE `domanda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferenze`
--

DROP TABLE IF EXISTS `preferenze`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferenze` (
  `fkIdUser` int(10) unsigned zerofill NOT NULL,
  `fkIdCategoria` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`fkIdUser`,`fkIdCategoria`),
  KEY `fkIdCategoria` (`fkIdCategoria`),
  CONSTRAINT `preferenze_ibfk_1` FOREIGN KEY (`fkIdCategoria`) REFERENCES `categoria` (`idCategoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `preferenze_ibfk_2` FOREIGN KEY (`fkIdUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferenze`
--

LOCK TABLES `preferenze` WRITE;
/*!40000 ALTER TABLE `preferenze` DISABLE KEYS */;
INSERT INTO `preferenze` VALUES (0000000001,0000000001),(0000000001,0000000002),(0000000001,0000000003),(0000000001,0000000004),(0000000001,0000000005),(0000000001,0000000006);
/*!40000 ALTER TABLE `preferenze` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `risposta`
--

DROP TABLE IF EXISTS `risposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risposta` (
  `fkIdSondaggio` int(11) unsigned zerofill NOT NULL,
  `fkIdDomanda` int(11) unsigned zerofill NOT NULL,
  `fkIdUser` int(11) unsigned zerofill NOT NULL,
  `r1` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fkIdSondaggio`,`fkIdDomanda`,`fkIdUser`),
  KEY `fkIdDomanda` (`fkIdDomanda`),
  KEY `fkIdUser` (`fkIdUser`),
  CONSTRAINT `risposta_ibfk_1` FOREIGN KEY (`fkIdDomanda`) REFERENCES `domanda` (`idDomanda`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `risposta_ibfk_2` FOREIGN KEY (`fkIdSondaggio`) REFERENCES `sondaggio` (`idSondaggio`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `risposta_ibfk_3` FOREIGN KEY (`fkIdUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `risposta`
--

LOCK TABLES `risposta` WRITE;
/*!40000 ALTER TABLE `risposta` DISABLE KEYS */;
INSERT INTO `risposta` VALUES (00000000002,00000000005,00000000001,'Si'),(00000000002,00000000006,00000000001,'Giotto di Bondone'),(00000000002,00000000007,00000000001,'Monna Lisa'),(00000000007,00000000015,00000000001,'No');
/*!40000 ALTER TABLE `risposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sondaggio`
--

DROP TABLE IF EXISTS `sondaggio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sondaggio` (
  `idSondaggio` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `dataCreazione` date NOT NULL,
  `dataScandenza` date NOT NULL,
  `fkIdCategoria` int(11) unsigned zerofill NOT NULL,
  `fkIdUser` int(11) unsigned zerofill NOT NULL,
  PRIMARY KEY (`idSondaggio`),
  KEY `fkIdCategoria` (`fkIdCategoria`),
  KEY `fkIdUser` (`fkIdUser`),
  CONSTRAINT `sondaggio_ibfk_1` FOREIGN KEY (`fkIdCategoria`) REFERENCES `categoria` (`idCategoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sondaggio_ibfk_2` FOREIGN KEY (`fkIdUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sondaggio`
--

LOCK TABLES `sondaggio` WRITE;
/*!40000 ALTER TABLE `sondaggio` DISABLE KEYS */;
INSERT INTO `sondaggio` VALUES (0000000001,'2015-04-02','2015-04-02',00000000004,00000000003),(0000000002,'2015-04-02','2016-04-02',00000000001,00000000003),(0000000003,'2015-04-02','2222-07-27',00000000004,00000000003),(0000000004,'2015-04-02','2020-03-30',00000000003,00000000003),(0000000005,'2015-04-02','2017-07-27',00000000006,00000000003),(0000000006,'2015-04-02','2222-07-27',00000000002,00000000003),(0000000007,'2015-04-02','2222-07-27',00000000005,00000000003);
/*!40000 ALTER TABLE `sondaggio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `idUser` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `eta` int(11) NOT NULL,
  `sesso` varchar(45) NOT NULL,
  `citta` varchar(45) NOT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0000000001,'Signor','Utente','user@web.com','user','user',55,'M','Roma','user'),(0000000002,'Signor','Platform','plat@web.com','platform','platform',33,'M','Milano','platform admin'),(0000000003,'Signora','Admin','admin@web.com','admin','admin',44,'F','Napoli','admin');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-02 14:34:37
