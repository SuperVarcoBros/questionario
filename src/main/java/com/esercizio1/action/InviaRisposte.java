package com.esercizio1.action;

import java.util.Map;

import com.esercizio1.model.User;
import com.esercizio1.services.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di invio di risposte scelte dall'utente nella
 * compilazione del sondaggio
 * 
 * @author Gruppo Silvi.
 *
 */
public class InviaRisposte extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private Map<Integer, String> radio;
	private UserService userService = new UserService();
	private User userBean;
	private int idSondaggio;

	@Override
	public String execute() throws Exception {
		// Prende la sessione dell'utente
		Map<String, Object> session = ActionContext.getContext().getSession();
		userBean = (User) session.get("user");
		System.out.println("idSondaggio" + idSondaggio);
		// userBean.setIdUser(userService.cercaIdUtenteDaUsernamee(userBean.getUsername()));
		System.out.println(userBean.toString() + "idUser: "
				+ userBean.getIdUser());
		if (userService.inviaRisposte(radio, userBean, idSondaggio))
			return SUCCESS;
		else
			return INPUT;
	}

	public Map<Integer, String> getRadio() {
		return radio;
	}

	public void setRadio(Map<Integer, String> radio) {
		this.radio = radio;
	}

	public void setIdSondaggio(int idSondaggio) {
		this.idSondaggio = idSondaggio;
	}

}
