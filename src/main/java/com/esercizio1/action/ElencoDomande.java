package com.esercizio1.action;

import java.util.List;

import com.esercizio1.model.Categoria;
import com.esercizio1.model.Domanda;
import com.esercizio1.services.AdminService;
import com.esercizio1.services.UserService;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di recupero dell'elenco delle domande
 * 
 * @author Gruppo Silvi.
 *
 */
public class ElencoDomande extends ActionSupport {

	private static final long serialVersionUID = 1L;
	// lista delle domande
	private List<Domanda> listaDomande = null;
	// lista delle categorie
	private List<Categoria> listaCategorie = null;
	private AdminService adminService = new AdminService();
	private UserService userService = new UserService();

	@Override
	public String execute() throws Exception {
		// prende la lista delle domande
		listaDomande = adminService.elencoDomande();
		// prende la lista delle categorie
		listaCategorie = userService.listaCategorie();
		// se la lista delle domande non � vuota
		if (listaDomande != null)
			// restituisce una stringa di successo
			return SUCCESS;
		// altrimenti restituisce una stringa di errore
		else
			return INPUT;
	}

	public List<Domanda> getListaDomande() {
		return listaDomande;
	}

	public List<Categoria> getListaCategorie() {
		return listaCategorie;
	}

}
