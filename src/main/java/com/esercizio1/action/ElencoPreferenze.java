package com.esercizio1.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.esercizio1.model.Categoria;
import com.esercizio1.model.Preferenza;
import com.esercizio1.model.User;
import com.esercizio1.services.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author Gruppo Silvi
 *
 */
public class ElencoPreferenze extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private UserService userService = new UserService();
	private User userBean;
	private List<Categoria> listaCategorie = null;
	private List<Categoria> listaPreferenze = null;

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public User getUserBean() {
		return userBean;
	}

	public void setUserBean(User userBean) {
		this.userBean = userBean;
	}

	public List<Categoria> getListaCategorie() {
		return listaCategorie;
	}

	public void setListaCategorie(List<Categoria> listaCategorie) {
		this.listaCategorie = listaCategorie;
	}

	public List<Categoria> getListaPreferenze() {
		return listaPreferenze;
	}

	public void setListaPreferenze(List<Categoria> listaPreferenze) {
		this.listaPreferenze = listaPreferenze;
	}

	public List<Preferenza> getListaPrefUtente() {
		return listaPrefUtente;
	}

	public void setListaPrefUtente(List<Preferenza> listaPrefUtente) {
		this.listaPrefUtente = listaPrefUtente;
	}

	private List<Preferenza> listaPrefUtente = null;

	@Override
	public String execute() throws Exception {
		Map<String, Object> session = ActionContext.getContext().getSession();
		userBean = (User) session.get("user");
		listaCategorie = userService.listaCategorie();
		listaPreferenze = userService.preferenzeUtente(userBean);
		listaPrefUtente = new ArrayList<Preferenza>();
		System.out.println("lista cat" + listaCategorie.toString());
		System.out.println("lista pref" + listaPreferenze.toString());
		for (Categoria c1 : listaCategorie) {
			listaPrefUtente.add(new Preferenza(c1, false));
		}
		for (Categoria c2 : listaPreferenze) {
			for (int i = 0; i < listaPrefUtente.size(); i++) {
				if (listaPrefUtente.get(i).getCategoria().getIdCategoria()==c2.getIdCategoria()) {
					listaPrefUtente.get(i).setValue(true);
				}
			}
		}
		System.out.println("lista pref bool" + listaPrefUtente.toString());
		if (listaPrefUtente != null)
			return SUCCESS;
		else
			return INPUT;
	}

	public void validate() {

	}
}
