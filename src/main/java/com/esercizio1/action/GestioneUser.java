package com.esercizio1.action;

import com.esercizio1.services.AdminPlatformService;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione della gestione degli utenti
 * 
 * @author Gruppo Silvi.
 *
 */
public class GestioneUser extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private AdminPlatformService adminPlatformService = new AdminPlatformService();
	private int idUser;

	public String cancellaUser() throws Exception {
		// Contrtolla che l'eliminazione sia andata a buon fine
		if (adminPlatformService.eliminaUtente(idUser))
			return SUCCESS;
		else
			return INPUT;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

}
