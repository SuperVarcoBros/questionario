package com.esercizio1.action;

import java.util.Map;

import com.esercizio1.model.Domanda;
import com.esercizio1.model.User;
import com.esercizio1.services.AdminService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione della creazione della domanda
 * 
 * @author Gruppo Silvi.
 *
 */
public class CreaDomanda extends ActionSupport {

	private static final long serialVersionUID = 1L;
	AdminService adminService = new AdminService();
	User userBean = null;
	String domanda, r1, r2, r3, r4;

	@Override
	public String execute() throws Exception {
		// Prende la sessione dell'utente loggato
		Map<String, Object> session = ActionContext.getContext().getSession();
		userBean = (User) session.get("user");
		// Stringa contenente le risposte alla domanda
		String[] risposte = new String[4];
		risposte[0] = r1;
		risposte[1] = r2;
		risposte[2] = r3;
		risposte[3] = r4;
		// Inserisce nella variabile question la domanda
		Domanda question = new Domanda(0, domanda, risposte,
				userBean.getIdUser()) {
		};
		// Prova a inserire la domanda del db
		if (adminService.creaDomanda(question, userBean) != null)
			// Se tutto � andato a buon fine restituisce una stringa di successo
			return SUCCESS;
		// altrimenti restitusce una stringa di errore
		else
			return INPUT;
	}

	public void setDomanda(String domanda) {
		this.domanda = domanda;
	}

	public void setR1(String r1) {
		this.r1 = r1;
	}

	public void setR2(String r2) {
		this.r2 = r2;
	}

	public void setR3(String r3) {
		this.r3 = r3;
	}

	public void setR4(String r4) {
		this.r4 = r4;
	}

}
