package com.esercizio1.action;

import java.util.List;

import com.esercizio1.model.Sondaggio;
import com.esercizio1.model.User;
import com.esercizio1.services.AdminService;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di visualizzazione dei sondaggi
 * 
 * @author Gruppo Silvi
 *
 */
public class VisualizzaSondaggi extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private AdminService adminService = new AdminService();
	private User userBean;

	private List<Sondaggio> visualizzaSondaggi = null;

	@Override
	public String execute() throws Exception {
		visualizzaSondaggi = adminService.visualizzaSondaggi();
		if (visualizzaSondaggi != null)
			return SUCCESS;

		else
			return INPUT;
	}

	public List<Sondaggio> getVisualizzaSondaggi() {
		return visualizzaSondaggi;
	}

	public User getUserBean() {
		return userBean;
	}

	public void setUserBean(User userBean) {
		this.userBean = userBean;
	}
}
