package com.esercizio1.action;

import com.esercizio1.services.AdminService;
import com.opensymphony.xwork2.ActionSupport;

public class ModificaSondaggio extends ActionSupport{

	private static final long serialVersionUID = 1L;
	
	private AdminService adminService = new AdminService(); 
			
	private int idSondaggio;
	
	public String cancellaSondaggio() throws Exception {
		
		if(adminService.cancellaSondaggio(idSondaggio))
			return SUCCESS;
		else return INPUT;
	}
	
	
	//public ModificaSondaggio(Sondaggio sondaggio){
	//	
	//}
	
	

	public int getIdSondaggio() {
		return idSondaggio;
	}

	public void setIdSondaggio(int idSondaggio) {
		this.idSondaggio = idSondaggio;
	}
	
	
}
