package com.esercizio1.action;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di logout
 * 
 * @author Gruppo Silvi
 *
 */
public class LogOut extends ActionSupport {
	private static final long serialVersionUID = 1L;
	Map<String, Object> session;

	@Override
	public String execute() {
		// Recupera la sessione dell'utente
		session = ActionContext.getContext().getSession();
		// la svuota
		session.clear();
		// controlla che sia vuota
		if (session.isEmpty())
			return SUCCESS;
		else
			return INPUT;
	}
}
