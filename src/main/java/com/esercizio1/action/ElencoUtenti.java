package com.esercizio1.action;

//import com.esercizio1.model.User;

import java.util.List;

import com.esercizio1.model.User;
import com.esercizio1.services.AdminPlatformService;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di recupero dell'elenco degli utenti
 * 
 * @author Gruppo Silvi.
 *
 */
public class ElencoUtenti extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private AdminPlatformService adminPlatformService = new AdminPlatformService();
	private List<User> elencoUtenti = null;

	@Override
	public String execute() throws Exception {
		// inserisce nella variabile l'elenco degli utenti
		elencoUtenti = adminPlatformService.elencoUtenti();
		// se la lista non � null
		if (elencoUtenti != null)
			// restituisce una stringa di successo
			return SUCCESS;
		// altrimenti di errore
		else
			return ERROR;
	}

	public List<User> getElencoUtenti() {
		return elencoUtenti;
	}

}
