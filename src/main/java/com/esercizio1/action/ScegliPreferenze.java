package com.esercizio1.action;

import java.util.Map;

import com.esercizio1.model.User;
import com.esercizio1.services.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di scelta delle preferenze della categoria
 * merceologica degli utenti
 * 
 * @author Gruppo Silvi
 *
 */
public class ScegliPreferenze extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private UserService userService = new UserService();
	private User userBean;
	private int idCategoria;

	public String aggiungiPreferenza() throws Exception {
		// esegui l'azione in base ad add/rem pref
		Map<String, Object> session = ActionContext.getContext().getSession();
		userBean = (User) session.get("user");
		if (userService.aggiungiPreferenza(idCategoria, userBean))
			return SUCCESS;
		else
			return INPUT;
	}

	public String rimuoviPreferenza() throws Exception {
		// esegui l'azione in base ad add/rem pref
		Map<String, Object> session = ActionContext.getContext().getSession();
		userBean = (User) session.get("user");
		if (userService.rimuoviPreferenza(idCategoria, userBean))
			return SUCCESS;
		else
			return INPUT;
	}

	public User getUserBean() {
		return userBean;
	}

	public void setUserBean(User userBean) {
		this.userBean = userBean;
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

}
