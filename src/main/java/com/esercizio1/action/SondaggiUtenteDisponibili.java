package com.esercizio1.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.esercizio1.model.Sondaggio;
import com.esercizio1.model.User;
import com.esercizio1.services.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di recupero dei sondaggi disponbili per gli
 * utenti
 * 
 * @author Gruppo Silvi
 *
 */
public class SondaggiUtenteDisponibili extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private UserService userService = new UserService();
	private List<Sondaggio> sondaggi = new ArrayList<Sondaggio>();

	public String execute() throws Exception {
		// Recupera la sessione dell'utente loggato
		Map<String, Object> session = ActionContext.getContext().getSession();
		User userBean = (User) session.get("user");
		// Assegna a sondaggi la lista dei sondaggi compilabili dall'utente
		// userBean
		sondaggi = userService.ricercaSondaggiDisponibili(userBean);
		// Se la lista non � null
		if (sondaggi != null)
			// Restituisce una stringa di successo
			return SUCCESS;
		// Altrimenti restutuisce una stringa di errore
		else
			return INPUT;
	}

	public List<Sondaggio> getSondaggi() {
		return sondaggi;
	}

}
