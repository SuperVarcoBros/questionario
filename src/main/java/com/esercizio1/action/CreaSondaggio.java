package com.esercizio1.action;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.esercizio1.model.Categoria;
import com.esercizio1.model.User;
import com.esercizio1.model.Domanda;
import com.esercizio1.services.AdminService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di creazione del sondaggio
 * 
 * @author Gruppo Silvi. 
 *
 */
public class CreaSondaggio extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private AdminService adminService = new AdminService();
	private User userBean = null;
	private Date dataCreazione, dataScadenza;
	// Contiene il nome della categoria selezionata
	private String nomeCategoria;
	// Contiene la lista di tutte le domande
	private List<Domanda> listaDomande;
	// Contiene la lista di tutte le categorie
	private List<Categoria> listaCategorie;
	// Contiene la selezione delle domande da aggiungere al questionario
	private Map<Integer, Boolean> checkboxes;

	@Override
	public String execute() throws Exception {
		// Prende la sessione dell'utente
		Map<String, Object> session = ActionContext.getContext().getSession();
		userBean = (User) session.get("user");
		// Inizializza con la data attuale
		java.util.Calendar cal = java.util.Calendar.getInstance();
		java.util.Date utilDate = cal.getTime();
		dataCreazione = new Date(utilDate.getTime());
		listaDomande = adminService.elencoDomande();
		System.out.println("NOME CATEGORIA SELEZIONATA: " + nomeCategoria);
		if (listaDomande != null) {
			// Se la creazione del sondaggio � danta a buon fine
			if (adminService.creaSondaggio(userBean, dataCreazione,
					dataScadenza,
					adminService.idCategoriaFromName(nomeCategoria)) != -1) {
				// Aggiungi le domande al sondaggio appena creato e controlla
				// che sia andato a buon fine
				if (adminService.aggiungiDomandeSondaggio(checkboxes,
						adminService.idSondaggioCorrente()))
					// Se tutto � andato a buon fine ritorna una stringa di
					// success
					return SUCCESS;
				return INPUT;
			}
			return INPUT;
		}
		// Altrimenti torna una stringa di errore
		else
			return INPUT;
	}

	public void setDataScadenza(Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	public List<Domanda> getListaDomande() {
		return listaDomande;
	}

	public Map<Integer, Boolean> getCheckboxes() {
		return checkboxes;
	}

	public void setCheckboxes(Map<Integer, Boolean> checkboxes) {
		this.checkboxes = checkboxes;
	}

	public List<Categoria> getListaCategorie() {
		return listaCategorie;
	}

	public void setListaCategorie(List<Categoria> listaCategorie) {
		this.listaCategorie = listaCategorie;
	}

	public String getNomeCategoria() {
		return nomeCategoria;
	}

	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}

}
