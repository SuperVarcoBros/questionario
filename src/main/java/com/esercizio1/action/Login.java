package com.esercizio1.action;

import java.util.Date;
import java.util.Map;

import com.esercizio1.model.User;
import com.esercizio1.services.PersonService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di login
 * 
 * @author Gruppo Silvi
 *
 */
public class Login extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private User userBean;
	private PersonService personService = new PersonService();

	@Override
	public String execute() throws Exception {
		userBean = personService.login(userBean);
		if (userBean != null) {
			// Sessione utente
			Map<String, Object> session = ActionContext.getContext()
					.getSession();
			session.put("user", userBean);
			session.put("context", new Date());
			// Restituisce il ruolo dell'utente loggato in modo da
			// reindirizzarlo
			// nella pagina giusta
			return userBean.getRole();
		}
		addFieldError("userBean.username", "wrong username and/or password");
		return INPUT;
	}

	public void validate() {
		if (userBean.getUsername().length() == 0)
			addFieldError("userBean.username", "Username is required.");
		if (userBean.getPassword().length() == 0)
			addFieldError("userBean.password", "Password is required");
	}

	public User getUserBean() {

		return userBean;

	}

	public void setUserBean(User user) {

		userBean = user;

	}

}
