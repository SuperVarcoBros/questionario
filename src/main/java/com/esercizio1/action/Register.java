package com.esercizio1.action;

import java.util.Map;

import com.esercizio1.services.PersonService;
import com.esercizio1.model.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di registrazione
 * 
 * @author Gruppo Silvi
 *
 */
public class Register extends ActionSupport {

	private static final long serialVersionUID = 1L;
	// Recupera la sessione dell'utente
	private Map<String, Object> session = ActionContext.getContext()
			.getSession();
	private User userBean = (User) session.get("user");
	private PersonService personService = new PersonService();

	@Override
	public String execute() throws Exception {
		// Controlla che la registrazione sia andata a buon fine
		if (personService.registrazione(userBean))
			return SUCCESS;
		else
			return INPUT;
	}

	public User getUserBean() {

		return userBean;

	}

	public void setUserBean(User user) {

		userBean = user;

	}

	public void validate() {

		if (userBean.getName().length() == 0) {

			addFieldError("userBean.name", "Name is required.");

		}

		if (userBean.getSurname().length() == 0) {
			addFieldError("userBean.surname", "Surname is required.");
		}

		if (userBean.getEmail().length() == 0) {

			addFieldError("userBean.email", "Email is required.");

		}

		if (userBean.getPassword().length() == 0) {

			addFieldError("userBean.password", "Password is required");

		}

		if (userBean.getUsername().length() == 0) {

			addFieldError("userBean.username", "UserName is required.");

		}

		if (userBean.getSesso().length() != 1) {

			addFieldError("userBean.sesso", "Sesso must be M or F.");

		}

		if (userBean.getCitta().length() == 0) {

			addFieldError("userBean.citta", "Citt� is required");

		}

		if (userBean.getEta() < 17) {

			addFieldError("userBean.eta",
					"Devi essere maggiorenne per registrarti");

		}

	}

}
