package com.esercizio1.action;

import java.util.List;

import com.esercizio1.model.AnalisiSondaggio;
import com.esercizio1.services.AdminService;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe che modella l'azione di visualizzazione dell'analisi dei sondaggi
 * 
 * @author Gruppo Silvi
 *
 */
public class VisualizzaAnalisiSondaggio extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	private int idSondaggio;
	private AdminService adminService = new AdminService();
	private List<AnalisiSondaggio> listaAnalisi = null;
	
	public String execute() throws Exception {
		listaAnalisi = adminService.risposteSondaggio(idSondaggio);
		if(listaAnalisi != null)
			return SUCCESS;
		else return INPUT;		
	}

	public int getIdSondaggio() {
		return idSondaggio;
	}
	
	public void setIdSondaggio(int idSondaggio) {
		this.idSondaggio = idSondaggio;
	}

	public List<AnalisiSondaggio> getListaAnalisi() {
		return listaAnalisi;
	}

	public void setListaAnalisi(List<AnalisiSondaggio> listaAnalisi) {
		this.listaAnalisi = listaAnalisi;
	}

}
