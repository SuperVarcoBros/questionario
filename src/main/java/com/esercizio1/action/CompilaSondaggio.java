package com.esercizio1.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.esercizio1.services.UserService;
import com.opensymphony.xwork2.ActionSupport;
import com.esercizio1.model.Domanda;

/**
 * 
 * Classe che modella l'azione compila sondaggio
 * 
 * @author Gruppo Silvi.
 *
 */
public class CompilaSondaggio extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private UserService userService = new UserService();
	private List<Domanda> listaDomande = new ArrayList<Domanda>();
	private int idSondaggio;
	// Contiene la selezione del questionario da compilare
	private Map<Integer, Boolean> checkboxes;

	public String execute() throws Exception {
		// Iteratore per scorrerre la mappa
		Iterator entries = checkboxes.entrySet().iterator();
		// Scorre tutti gli elementi della mappa
		while (entries.hasNext()) {
			Entry thisEntry = (Entry) entries.next();
			// key contiene idQuestionario
			int key = (Integer) thisEntry.getKey();
			// value indica se idQuestionario � stato selezionato
			boolean value = (Boolean) thisEntry.getValue();
			System.out.println("Intero: " + key + " valore: " + value);
			// Se il value � true il questionario � stato selezionato
			if (value == true) {
				// Assegna a idSondaggio la chiave del sondaggi selezionato
				idSondaggio = key;
				break;
			}
		}
		listaDomande = userService.popolaSondaggio(idSondaggio);
		if (listaDomande == null) {
			return INPUT;
		}
		return SUCCESS;
	}

	public List<Domanda> getListaDomande() {
		return listaDomande;
	}

	public void setIdSondaggio(int idSondaggio) {
		this.idSondaggio = idSondaggio;
	}

	public int getIdSondaggio() {
		return idSondaggio;
	}

	public Map<Integer, Boolean> getCheckboxes() {
		return checkboxes;
	}

	public void setCheckboxes(Map<Integer, Boolean> checkboxes) {
		this.checkboxes = checkboxes;
	}

}
