package com.esercizio1.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.esercizio1.model.Categoria;
import com.esercizio1.model.User;
import com.esercizio1.services.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PreferenzeCheckedBoxListAction extends ActionSupport{

	
	private static final long serialVersionUID = 1L;

	private List<String> categorie;
	
	private String categoria;
	
	private UserService userService = new UserService();
	
	private User userBean;
	
	public PreferenzeCheckedBoxListAction() throws Exception{
		List<Categoria> listaCategorie = userService.listaCategorie();
		categorie = new ArrayList<String>();
		for (Categoria c1 : listaCategorie) {
			categorie.add(c1.getName());
		}
	}
		
	public String[] getDefaultCategorie() throws Exception{
		Map<String, Object> session = ActionContext.getContext().getSession();
		userBean = (User) session.get("user");
		List<Categoria> listaPreferenze = userService.preferenzeUtente(userBean);
		String[] preferenze = new String[listaPreferenze.size()];
		for (int i = 0; i < preferenze.length; i++) {
			preferenze[i]= listaPreferenze.get(i).getName();	
		}
		return preferenze;
	}
	
	
	public String execute() throws ClassNotFoundException, SQLException{
		Map<String, Object> session = ActionContext.getContext().getSession();
		userBean = (User) session.get("user");
		userService.aggiungiPreferenze(getCategoria(), userBean);
		return SUCCESS;
	}
	
	public String display(){
		return NONE;
	}
	
	public List<String> getCategorie() {
		return categorie;
	}
	
	public void setCategorie(List<String> categorie) {
		this.categorie = categorie;
	}
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public User getUserBean() {
		return userBean;
	}

	public void setUserBean(User userBean) {
		this.userBean = userBean;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
}
