package com.esercizio1.services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.esercizio1.model.Categoria;
import com.esercizio1.model.Domanda;
import com.esercizio1.model.Sondaggio;
import com.esercizio1.model.User;

/**
 * Questa classe contiene tutti i servizi dello user
 * 
 * @author Gruppo Silvi.
 *
 */
public class UserService extends PersonService {

	/**
	 * 
	 * @param userBean
	 *            utente da cui prendere le preferenze
	 * @return lista delle categorie scelte dall'utente
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo visualizza tutte le preferenze di un utente
	 */
	public List<Categoria> preferenzeUtente(User userBean)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		String sql = "";
		ResultSet rs;
		ArrayList<Categoria> categorie = new ArrayList<Categoria>();
		Connection conn = null;
		try {
			conn = getConnection();
			sql = ("SELECT idCategoria,name FROM categoria,preferenze WHERE fkIdUser ='"
					+ userBean.getIdUser() + "' AND fkIdCategoria = idCategoria");
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				categorie.add(new Categoria(rs.getInt("idCategoria"), rs
						.getString("name")));
			}
			return categorie;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param nomiPreferenze
	 *            nome della preferenza scelta dall'utente
	 * @param userBean
	 *            cui aggiungere la preferenza
	 * @return Boolean true se � andato a buon fine, false altrimenti
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo aggiunge le preferenze scelte dall'utente
	 */
	public boolean aggiungiPreferenze(String nomiPreferenze, User userBean)
			throws ClassNotFoundException, SQLException {
		// Rimuove tutte le preferenze dell'utente
		rimuoviAllPreferenza(userBean);
		String[] nomiPrefArray = nomiPreferenze.split(", ");
		PreparedStatement ps = null;
		String sql = "";
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			for (int i = 0; i < nomiPrefArray.length; i++) {
				sql = ("SELECT  idCategoria FROM categoria WHERE name = '"
						+ nomiPrefArray[i] + "'");
				ps = conn.prepareStatement(sql);
				rs = ps.executeQuery();
				rs.next();
				int idCat = rs.getInt("idCategoria");
				// Aggiunge tutte le preferenze scelte dall'utente
				aggiungiPreferenza(idCat, userBean);
			}
			// restituisce true se tutto � andato a buon fine
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			// false altrimenti
			return false;
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param idCategoria
	 *            da aggiungere all'utente
	 * @param userBean
	 *            cui aggiungere la preferenza
	 * @return Boolean true se tutto � andato a buon fine, false altrimenti
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo inserisce le preferenze dell'utente nel db
	 */
	public boolean aggiungiPreferenza(int idCategoria, User userBean)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		String sql = "";
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			// seleziona le categorie in base all'id passato
			sql = ("SELECT * FROM preferenze WHERE fkIdCategoria = ? AND fkIdUser = ? ");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, idCategoria);
			ps.setInt(2, userBean.getIdUser());
			rs = ps.executeQuery();
			if (!rs.next()) {
				// le aggiunge al db
				sql = ("INSERT INTO preferenze (fkIdUser, fkIdCategoria) "
						+ "VALUES(?, ?)");
				PreparedStatement pss = null;
				pss = conn.prepareStatement(sql);
				pss.setInt(1, userBean.getIdUser());
				pss.setInt(2, idCategoria);
				pss.executeUpdate();
				if (pss != null)
					pss.close();
			}
			// restituisce true
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param userBean
	 *            cui rimuovere tutte le preferenze
	 * @return True se � andato tutto a buon fine, false altrimenti
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo rimuove tutte le preferenze dell'utente
	 */
	public boolean rimuoviAllPreferenza(User userBean)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		try {
			conn = getConnection();
			sql = ("DELETE FROM preferenze WHERE fkIdUser = ?");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userBean.getIdUser());
			System.out.println(ps);
			ps.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param idCategoria
	 *            da rimuovere dalle preferenze
	 * @param userBean
	 *            cui rimouovere la preferenza
	 * @return Boolean true se tutto � andato a buon fine false altrimenti
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 */
	public boolean rimuoviPreferenza(int idCategoria, User userBean)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		try {
			conn = getConnection();
			sql = ("DELETE FROM preferenze WHERE fkIdUser = ? AND fkIdCategoria = ?");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userBean.getIdUser());
			ps.setInt(2, idCategoria);
			System.out.println(ps);
			ps.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @return lista di tutte le categorie disponibili
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             QUesto metodo restituisce tutte le categorie merceologiche
	 *             disponibili
	 */
	public List<Categoria> listaCategorie() throws ClassNotFoundException,
			SQLException {
		PreparedStatement ps = null;
		String sql = "";
		ResultSet rs;
		ArrayList<Categoria> categorie = new ArrayList<Categoria>();
		Connection conn = null;
		try {
			conn = getConnection();
			sql = "SELECT * FROM categoria";
			ps = conn.prepareStatement(sql);
			System.out.println(ps);
			rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getInt("idCategoria"));
				System.out.println(rs.getString("name"));
				categorie.add(new Categoria(rs.getInt("idCategoria"), rs
						.getString("name")));
			}
			return categorie;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param user
	 *            che ha fatto la richiesta
	 * @return Lista dei sondaggi disponibili per l'utente
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo restituisce la lista dei sondaggi di un
	 *             determinato utente
	 */
	public List<Sondaggio> ricercaSondaggiDisponibili(User user)
			throws ClassNotFoundException, SQLException {
		List<Sondaggio> sondaggi = new ArrayList<Sondaggio>();
		List<Categoria> preferenzeUtente = new ArrayList<Categoria>();
		// Imposta la data odiernna
		java.util.Calendar cal = java.util.Calendar.getInstance();
		java.util.Date utilDate = cal.getTime();
		Date dataOdierna = new Date(utilDate.getTime());
		int i = 0;
		// Assegna alla lista le preferenze dell'utente
		preferenzeUtente = preferenzeUtente(user);
		PreparedStatement ps = null;
		String sql = "";
		ResultSet rs;
		Connection conn = null;
		// Scorre la lista delle preferenze dell'utente
		while (preferenzeUtente.size() > i) {
			try {
				// Per ogni preferenza dell'utente controlla che ci siano
				// sondaggi disponibili non chiusi(scaduti)
				conn = getConnection();
				sql = "SELECT * FROM sondaggio WHERE fkIdCategoria = ? AND dataScandenza> ? and idSondaggio not in (select sondaggio.idSondaggio from sondaggio,risposta WHERE risposta.fkIdUser= ? and sondaggio.idSondaggio = risposta.fkIdSondaggio group by risposta.fkIdSondaggio)";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, preferenzeUtente.get(i).getIdCategoria());
				ps.setDate(2, dataOdierna);
				ps.setInt(3, user.getIdUser());
				System.out.println("QUERYYYYYYYYYYYYYYYYY" + ps);
				rs = ps.executeQuery();
				// Inserisci i risultati della query nella lista sondaggi
				while (rs.next()) {
					sondaggi.add(new Sondaggio(rs.getInt("idSondaggio"), 0, rs
							.getDate("dataCreazione"), rs
							.getDate("dataScandenza"), new Categoria(rs
							.getInt("fkIdCategoria"), " ")));
				}
			} // Se ci sono stati errori restituisci null
			catch (Exception e) {
				e.printStackTrace();
				return null;
			} finally {
				// Chiude le connessioni
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
			}
			// Prossimo elemento
			i++;
		}
		// Restituisce la lista dei sondaggi disponibili per l'utente
		return sondaggi;
	}

	/**
	 * 
	 * @param idSondaggio
	 *            da cui prendere le domande
	 * @return domande appartenenti al sondaggio
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Restituisce tutte le domande di un determinato sondaggio
	 */
	public List<Domanda> popolaSondaggio(int idSondaggio)
			throws ClassNotFoundException, SQLException {
		List<Domanda> domande = new ArrayList<Domanda>();
		PreparedStatement ps = null;
		String sql = "";
		ResultSet rs = null;
		Connection conn = null;
		// Prova a selezionare le domande appartenenti alla categoria scelta
		try {
			conn = getConnection();
			sql = ("SELECT * FROM sondaggio, composto, domanda WHERE fkIdSondaggio = ? AND fkIdDomanda = idDomanda GROUP BY idDomanda");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, idSondaggio);
			System.out.println(ps);
			rs = ps.executeQuery();
			// Aggiunge le domande del sondaggio selezionato dallo user alla
			// lista domande
			while (rs.next()) {
				// Domanda Si/no
				if (rs.getString("r1").equals("")) {
					String[] risposte = new String[2];
					risposte[0] = "Si";
					risposte[1] = "No";
					domande.add(new Domanda(rs.getInt("idDomanda"), rs
							.getString("domanda"), risposte, rs
							.getInt("fkIdUser")) {
					});
				}
				// Domanda risposta multipla
				else {
					String[] risposte = new String[4];
					risposte[0] = rs.getString("r1");
					risposte[1] = rs.getString("r2");
					risposte[2] = rs.getString("r3");
					risposte[3] = rs.getString("r4");
					domande.add(new Domanda(rs.getInt("idDomanda"), rs
							.getString("domanda"), risposte, rs
							.getInt("fkIdUser")) {
					});
				}
			}
			// Se si verifica un'eccezione restituisci null
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// Chiude le connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
		// Se tutto � andato a abuon fine restituisce la lista
		return domande;
	}

	/**
	 * 
	 * @param radio
	 *            Mappa che contiene gli id delle domande e le relative risposte
	 * @param user
	 *            che ha compilato il sondaggio
	 * @param idSondaggio
	 *            compilato
	 * @return Boolean true se � andato tutto a buon fine, false altrimenti
	 * @throws SQLException
	 *             eccezione sql
	 * @throws ClassNotFoundException
	 *             eccezione
	 * 
	 *             Questo metodo inserisce le risposte date dall'utente nel db
	 */
	public Boolean inviaRisposte(Map<Integer, String> radio, User user,
			int idSondaggio) throws SQLException, ClassNotFoundException {
		Iterator<Entry<Integer, String>> entries = radio.entrySet().iterator();
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		// Finch� la mappa non � vuota
		while (entries.hasNext()) {
			// Itera la mappa
			Entry<Integer, String> thisEntry = (Entry<Integer, String>) entries
					.next();
			try {
				conn = getConnection();
				// ogni valore della mappa viene inserito nel db
				sql = ("INSERT INTO risposta (fkIdSondaggio, fkIdDomanda, fkIdUser, r1) "
						+ "VALUES(?, ?, ?, ?)");
				ps = conn.prepareStatement(sql);
				ps.setInt(1, idSondaggio);
				ps.setInt(2, (Integer) thisEntry.getKey());
				ps.setInt(3, user.getIdUser());
				ps.setString(4, (String) thisEntry.getValue());
				System.out.println(ps);
				ps.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		if (conn != null)
			conn.close();
		if (ps != null)
			ps.close();
		return true;
	}

}
