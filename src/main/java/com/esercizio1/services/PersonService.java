package com.esercizio1.services;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.esercizio1.model.User;

/**
 * Questa classe contiene i servizi di person, superclasse di admin, platform e
 * user
 * 
 * @author Gruppo Silvi.
 *
 */
public class PersonService {

	Properties prop = new Properties();
	InputStream is = this.getClass().getResourceAsStream(
			"/stringheConnessione.proprerties");
	private String URL;
	private String USER;
	private String PSW;
	private String DRIVER;

	// Costruttore che inizializza le stringhe di connessione
	public PersonService() {
		try {
			prop.load(is);
			this.URL = prop.getProperty("url");
			this.DRIVER = prop.getProperty("driver");
			this.USER = prop.getProperty("user");
			this.PSW = prop.getProperty("psw");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Apre le connessioni con il db mysql
	public Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PSW);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return conn;
	}

	/**
	 * 
	 * @param userBean
	 *            user che sta effetttuando il login
	 * @return user che ha effettuato il login
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Queesto metodo permeette il login agli utenti
	 */
	public User login(User userBean) throws ClassNotFoundException,
			SQLException {
		PreparedStatement ps = null;
		String sql = "";
		ResultSet rs;
		Connection conn = null;
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PSW);
			// query
			sql = "SELECT * FROM user WHERE username = ? and password=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, userBean.getUsername());
			ps.setString(2, userBean.getPassword());
			// esegue la query
			rs = ps.executeQuery();
			// sposta il resultset sul primo risultato valido
			rs.next();
			// inserisce nella variabile user i dati dell'utente loggato
			User user = new User(rs.getInt("idUser"), rs.getString("name"),
					rs.getString("surname"), rs.getString("email"),
					rs.getString("username"), rs.getString("password"),
					rs.getInt("eta"), rs.getString("sesso"),
					rs.getString("citta"), rs.getString("role"));
			// restituisce lo user appena loggato
			return user;
			// nel caso ci siano errori lanciaa un'eccezione
		} catch (Exception e) {
			e.printStackTrace();
			// e ritorna null
			return null;
			// chiude le connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param userBean
	 *            user che vuole registrarsi
	 * @return true se va a buon fine, false altrimenti
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo permette di rergistrare un nuovo utente
	 */
	public boolean registrazione(User userBean) throws ClassNotFoundException,
			SQLException {
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		// Controllo sull'esistenza dello username
		if (!ceckUsername(userBean.getUsername())) {
			try {
				Class.forName(DRIVER);
				conn = DriverManager.getConnection(URL, USER, PSW);
				// query
				sql = ("INSERT INTO user (name, surname, email, username, password, eta, sesso, citta, role) "
						+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
				ps = conn.prepareStatement(sql);
				ps.setString(1, userBean.getName());
				ps.setString(2, userBean.getSurname());
				ps.setString(3, userBean.getEmail());
				ps.setString(4, userBean.getUsername());
				ps.setString(5, userBean.getPassword());
				ps.setInt(6, userBean.getEta());
				ps.setString(7, userBean.getSesso());
				ps.setString(8, userBean.getCitta());
				ps.setString(9, userBean.getRole());
				// esegue la query
				ps.executeUpdate();
				// restituisce true
				return true;
				// nel caso ci siano errori lancia un eccezione
			} catch (Exception e) {
				e.printStackTrace();
				// e restituisce false
				return false;
				// chiude le connessioni
			} finally {
				if (conn != null)
					conn.close();
				if (ps != null)
					ps.close();
			}
		} else
			return false;
	}

	/**
	 * 
	 * @param username
	 *            da verificare la presenza nel db
	 * @return true se lo username esiste gia, false altriment
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metod controlla che lo username non sia gia presente
	 *             nel db
	 */
	public boolean ceckUsername(String username) throws ClassNotFoundException,
			SQLException {
		PreparedStatement ps = null;
		String sql = "";
		ResultSet rs;
		// Vale true se lo username esiste
		boolean status = false;
		Connection conn = null;
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PSW);
			sql = "SELECT * FROM user WHERE username = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			rs = ps.executeQuery();
			// se il resultset non � vuoto
			if (rs.next()) {
				// controlla se lo username esiste
				if (rs.getString("username").equals(username))
					// se esiste restituisce true
					status = true;
			}
			// false altrimenti
			return status;
			// lanciaa l'eccezione nel caso di errori
		} catch (Exception e) {
			e.printStackTrace();
			// e restituisce true
			return true;
			// chiude le connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

}
