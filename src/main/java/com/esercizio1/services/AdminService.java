package com.esercizio1.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.sql.Date;
import java.util.List;

import com.esercizio1.model.AnalisiSondaggio;
import com.esercizio1.model.Categoria;
import com.esercizio1.model.Domanda;
import com.esercizio1.model.Sondaggio;
import com.esercizio1.model.User;
import com.esercizio1.model.Risposta;

/**
 * Questa classe contiene tutti i servizi dell'amministratore
 * 
 * @author Gruppo Silvi.
 *
 */
public class AdminService extends PersonService {

	/**
	 * 
	 * @param domanda
	 *            da inserire nel db
	 * @param user
	 *            che ha creato la domanda
	 * @return restituisce la domanda appena creata
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo inserisce una nuova domanda all'interno del db
	 */
	public Domanda creaDomanda(Domanda domanda, User user)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		String sql = "";
		// Array che conterr� le possibili risposte alla domanda
		String risposte[] = new String[4];
		// Inserisce nell'array le risposte presenti in domanda
		risposte = domanda.getRisposte();
		Connection conn = null;
		try {
			conn = getConnection();
			// Query
			sql = ("INSERT INTO domanda (fkIdUser, domanda, r1, r2, r3, r4) "
					+ "VALUES(?, ?, ?, ?, ?, ?)");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getIdUser());
			ps.setString(2, domanda.getDomanda());
			ps.setString(3, risposte[0]);
			ps.setString(4, risposte[1]);
			ps.setString(5, risposte[2]);
			ps.setString(6, risposte[3]);
			// Esegue la query
			ps.executeUpdate();
			// Restituisce la domanda appena creata
			return domanda;
			// Lancia l'eccezione nel caso ci siano errori
		} catch (Exception e) {
			e.printStackTrace();
			// E restituisce null
			return null;
			// Chiude le connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @return lista di tutti i sondaggi presenti nel db
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo restittuisce iuna lista di tutti i sondaggi
	 *             presenti nel db
	 */
	public List<Sondaggio> visualizzaSondaggi() throws ClassNotFoundException,
			SQLException {
		PreparedStatement ps = null;
		String sql = "";
		// lista che conterr� tutti i sondagi
		ArrayList<Sondaggio> sondaggio = new ArrayList<Sondaggio>();
		Connection conn = null;
		Categoria categoria;
		try {
			conn = getConnection();
			// query
			sql = ("SELECT * FROM sondaggio ");
			ps = conn.prepareStatement(sql);
			// esegue la query
			ResultSet rs = ps.executeQuery();
			// scorre la lista dei resulset e per ogni elemento lo aggiunge in
			// lista
			while (rs.next()) {
				categoria = new Categoria();
				categoria.setIdCategoria(rs.getInt("fkIdCategoria"));
				categoria.setName(nameCategoria(categoria.getIdCategoria()));
				sondaggio.add(new Sondaggio(rs.getInt("idSondaggio"), rs
						.getInt("fkIdUser"), rs.getDate("dataCreazione"), rs
						.getDate("dataScandenza"), categoria));
			}
			// Restituisce la lista dei sondaggi
			return sondaggio;
		}
		// Nel caso c siano errori lanca un eccezione
		catch (Exception e) {
			e.printStackTrace();
			// e restituisce null
			return null;
			// chiude le connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * @param user
	 *            che crea il sondaggio
	 * @param dataCreazione
	 *            data creazione del sondaggio
	 * @param dataScadenza
	 *            data scadenza del sondaggio
	 * @param idCategoria
	 *            id della categoria del sondaggio
	 * @return Integer id del sondaggio creato maggiore di 0 se � andato a buon
	 *         fine
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo crea un nuovo sondaggio
	 */
	public int creaSondaggio(User user, Date dataCreazione, Date dataScadenza,
			int idCategoria) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		try {
			conn = getConnection();
			// Query
			sql = ("INSERT INTO sondaggio (fkIdUser, fkIdCategoria, dataCreazione, dataScandenza) "
					+ "VALUES(?, ?, ?, ?)");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getIdUser());
			ps.setInt(2, idCategoria);
			ps.setDate(3, dataCreazione);
			ps.setDate(4, dataScadenza);
			// esegue la query
			ps.executeUpdate();
			// restituisce l'id del sondaggio appena creato
			return idSondaggioCorrente();
			// nel caso in cui ci siano errori lancia un eccezione
		} catch (Exception e) {
			e.printStackTrace();
			// e restituisce -1
			return -1;
			// chiude le cnnessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param idCategoria
	 *            id della categoria
	 * @return nome della categoria
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo restituisce il nome della categora dato il suo
	 *             id
	 */
	public String nameCategoria(int idCategoria) throws ClassNotFoundException,
			SQLException {
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		String categoria = null;
		try {
			conn = getConnection();
			// query
			sql = ("SELECT name FROM categoria where idCategoria= ? ");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, idCategoria);
			// esegue la query
			ResultSet rs = ps.executeQuery();
			// muove il result set sull'ultimo risultato utile
			while (rs.next())
				// e assegna a categoria il nome cercato
				categoria = (rs.getString("name"));
			// restutiusce il nome della categoria
			return categoria;
		}
		// nel caso ci siano errori lanncia un eccezione
		catch (Exception e) {
			e.printStackTrace();
			// e restituisce null
			return null;
			// chiude le connessioni con il db
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}

	}

	/**
	 * 
	 * @return lista di tutte le domande presenti nel db
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo restituisce una lista di tutte le domande
	 *             presenti nel db
	 */
	public List<Domanda> elencoDomande() throws ClassNotFoundException,
			SQLException {
		PreparedStatement ps = null;
		String sql = "";
		// Lista che sar� popolata dalle domande trovate nel db
		ArrayList<Domanda> domande = new ArrayList<Domanda>();
		Connection conn = null;
		try {
			conn = getConnection();
			// query
			sql = ("SELECT * FROM domanda");
			ps = conn.prepareStatement(sql);
			// esegue la query
			ResultSet rs = ps.executeQuery();
			// scorre la lista dei resulset e inserisce ogni elemento nella
			// lista domande
			while (rs.next()) {
				// crea una vettore di domande da inserire all'interno della
				// lista
				String[] risposte = new String[4];
				risposte[0] = rs.getString("r1");
				risposte[1] = rs.getString("r2");
				risposte[2] = rs.getString("r3");
				risposte[3] = rs.getString("r4");
				domande.add(new Domanda(rs.getInt("idDomanda"), rs
						.getString("domanda"), risposte, rs.getInt("fkIdUser")) {
				});
			}
			// restituisce la lista
			return domande;
			// nel caso ci siano eccezzioni, vengono lanciate
		} catch (Exception e) {
			e.printStackTrace();
			// e restituisce null
			return null;
			// chiude le connessini
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param domande
	 *            lista delle domande da aggiungere al sondaggio
	 * @param idSondaggio
	 *            id del sondaggio cui aggiungere le domande
	 * @return true se tutto � andato a buon fine, false altrimenti
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo inserisce una lista delle domande all'interno
	 *             di un sondaggio creato in precedenza
	 */
	public Boolean aggiungiDomandeSondaggio(Map<Integer, Boolean> domande,
			int idSondaggio) throws ClassNotFoundException, SQLException {
		// Iteratore per scorrerre la mappa
		Iterator<Entry<Integer, Boolean>> entries = domande.entrySet()
				.iterator();
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		// Scorre la lista delle domande
		while (entries.hasNext()) {
			Entry<Integer, Boolean> thisEntry = (Entry<Integer, Boolean>) entries
					.next();
			// key contiene idDomanda
			int key = (Integer) thisEntry.getKey();
			// value indica se idDomanda va aaggiunto o meno
			boolean value = (Boolean) thisEntry.getValue();
			if (value == true) {
				try {
					conn = getConnection();
					// Per ogni elemento true, esegue una query per aggiungerla
					// nella tabella "composto"
					sql = ("INSERT INTO composto (fkIdSondaggio, fkIdDomanda) VALUES(?, ?)");
					ps = conn.prepareStatement(sql);
					ps.setInt(1, idSondaggio);
					ps.setInt(2, key);
					System.out.println("QUERYYYYYYYYYYYYYYYYYYYYYYYYY" + ps);
					// Esegue la query
					ps.executeUpdate();
				} catch (Exception e) {
					// Se si � verificato un'eccezione ritorna falso
					e.printStackTrace();
					return false;
				}
			}
		}
		// Chiude le connssioni
		if (conn != null)
			conn.close();
		if (ps != null)
			ps.close();
		// Se tutto � andato a buon fine ritorna true
		return true;
	}

	/**
	 * 
	 * @return Integer id dell'ultimo sondaggio creato maggiore di 0 in caso sia
	 *         andato a buon fine
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo restituisce l'id dell'ultimo sondaggo creato
	 */
	public int idSondaggioCorrente() throws ClassNotFoundException,
			SQLException {
		// variabile che conterr� l'id del sondaggio
		int idSondaggio = 0;
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		try {
			conn = getConnection();
			// query
			sql = ("SELECT idSondaggio FROM sondaggio ORDER BY idSondaggio DESC LIMIT 1");
			ps = conn.prepareStatement(sql);
			// esegue la query
			ResultSet rs = ps.executeQuery();
			// sposta il resultset all'ultimo risultato utile (� al pi� uno)
			while (rs.next())
				// inseerisce in idsondaggio l'id del sondaggio trovato
				idSondaggio = rs.getInt("idSondaggio");
			// e lo restutiusce
			return idSondaggio;
			// eccezione
		} catch (Exception e) {
			e.printStackTrace();
			// restutiusce un valore negativo
			return -1;
			// chiude connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param nomeCategoria
	 *            nome della categoria da cercare
	 * @return id della categoria trovato maggiore di 0 se tutto � aandato a
	 *         buon fine
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo restituisce l'id della categoria dato il nome
	 *             di quest'ultima
	 */
	public int idCategoriaFromName(String nomeCategoria)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		// variabile che conterr� l'id della categoria da cercare
		int idCategoria = -1;
		try {
			conn = getConnection();
			// query
			sql = ("SELECT idCategoria FROM categoria where name= ? ");
			ps = conn.prepareStatement(sql);
			ps.setString(1, nomeCategoria);
			// esegue la query
			ResultSet rs = ps.executeQuery();
			// posiziona il resultset all'ultimo risultato valido
			while (rs.next())
				// inserisce nella variabile l'id trovato
				idCategoria = (rs.getInt("idCategoria"));
			// rerstutiusce l'id
			return idCategoria;
			// lancia l'eccezione nel caso ci siano problemi
		} catch (Exception e) {
			e.printStackTrace();
			// e ritorna un valore negativo
			return -1;
			// chiude le cconnessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param idSondaggio
	 *            da cancellare
	 * @return true se � andato a buon fine, false altrimenti
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo cancella un sondaggio dato il suo id
	 */
	public boolean cancellaSondaggio(int idSondaggio)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		try {
			conn = getConnection();
			// query
			sql = ("DELETE FROM sondaggio WHERE idSondaggio = ?");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, idSondaggio);
			// esegue la query
			ps.executeUpdate();
			// restituisce true se tutto � andato abuon fine
			return true;
			// lancia un eccezione nel caso ci siano errori
		} catch (Exception e) {
			e.printStackTrace();
			// e restituisce false
			return false;
			// chiude le connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param idSondaggio
	 *            di cui si volgiono analizzare le risposte
	 * @return lista delle risposte date al sondaggio
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo restituisce l'analisi di un sondaggio
	 */
	public List<AnalisiSondaggio> risposteSondaggio(int idSondaggio)
			throws ClassNotFoundException, SQLException {
		PreparedStatement ps = null;
		String sql = "";
		ArrayList<AnalisiSondaggio> analisiSondaggio = new ArrayList<AnalisiSondaggio>();
		Connection conn = null;
		try {
			conn = getConnection();
			// query
			sql = ("SELECT fkIdSOndaggio, fkIdDomanda, r1, count(r1) FROM risposta "
					+ "WHERE fkIdSondaggio = ? " + "GROUP BY fkIdDomanda , r1");
			ps = conn.prepareStatement(sql);
			// //set id sondaggio, per il quale si vuole avere l'analisi delle
			// risposte
			ps.setInt(1, idSondaggio);
			System.out.println(ps);
			// ps torna la query corretta////
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				// per ogni risultato aggiunge un elemento in lista
				analisiSondaggio.add(new AnalisiSondaggio(rs
						.getInt("fkIdSondaggio"), rs.getInt("fkIdDomanda"), rs
						.getString("r1"), rs.getInt("count(r1)")));
			//restituisce la lista
			return analisiSondaggio;
			// lancia un eccezione nel caso ci siano errori
		} catch (Exception e) {
			e.printStackTrace();
			// e restituisce false
			return null;
			// chiude le connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

}
