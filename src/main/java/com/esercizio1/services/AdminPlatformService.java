package com.esercizio1.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.esercizio1.model.User;

/**
 * In questa classe sono presenti tutti i servizi dell'admin platform
 * 
 * @author Gruppo Silvi.
 *
 */
public class AdminPlatformService extends PersonService {

	// cerca gli utenti data una sottostringa
	/**
	 * 
	 * @param chiave
	 *            String con cui cercare gli utenti
	 * @return Lista di utenti trovavti
	 * @throws SQLException
	 *             eccezione sql
	 * @throws ClassNotFoundException
	 *             eccezione eccezione sql
	 * 
	 *             Restituisce una lista degli utenti cercati
	 */
	public List<User> cercaUtente(String chiave) throws ClassNotFoundException,
			SQLException {
		// Lista degli utenti trovati
		ArrayList<User> utentiTrovati = new ArrayList<User>();
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		try {
			conn = getConnection();
			// La query cerca per nome, cognome, username e ruolo
			sql = ("SELECT * FROM user WHERE name LIKE %?% OR surname LIKE %?% OR username LIKE %?% OR role LIKE %?%");
			ps = conn.prepareStatement(sql);
			ps.setString(1, chiave);
			ps.setString(2, chiave);
			ps.setString(3, chiave);
			ps.setString(4, chiave);
			// Eseggue la query
			ResultSet rs = ps.executeQuery();
			// Scorre la lista dei result set e per ogni tupla aggiunge un
			// utente alla lista
			while (rs.next()) {
				utentiTrovati.add(new User(rs.getInt("idUser"), rs
						.getString("name"), rs.getString("surname"), rs
						.getString("email"), rs.getString("username"), rs
						.getString("password"), rs.getInt("eta"), rs
						.getString("sesso"), rs.getString("citta"), rs
						.getString("role")));
			}
			// Restituisce gli utenti trovati
			return utentiTrovati;
			// Lancia eccezione nel caso qualcosa sia andato storto
		} catch (Exception e) {
			e.printStackTrace();
			return utentiTrovati;
			// Chiude le connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param idUtenteDaModificare
	 *            id dell'utente cui modificare i dati
	 * @param datiUtenteModificato
	 *            nuovi dati da sotituire ai vecchi
	 * @return true se � andato a buon fine, false altrimenti
	 * @throws SQLException
	 *             eccezione sql
	 * @throws ClassNotFoundException
	 *             eccezione
	 * 
	 *             Questo metodo modifica i dati di un utente
	 */
	public boolean modificaDatiUtente(int idUtenteDaModificare,
			User datiUtenteModificato) throws SQLException,
			ClassNotFoundException {
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		try {
			conn = getConnection();
			// Query che esegue l'upadate all'interno del database
			sql = ("UPDATE user SET name=?, surname=?, email=?, username=?, password=?, eta=?, sesso=?, citta=?, ruolo=?");
			ps = conn.prepareStatement(sql);
			ps.setString(1, datiUtenteModificato.getName());
			ps.setString(2, datiUtenteModificato.getSurname());
			ps.setString(3, datiUtenteModificato.getEmail());
			ps.setString(4, datiUtenteModificato.getUsername());
			ps.setString(5, datiUtenteModificato.getPassword());
			ps.setInt(6, datiUtenteModificato.getEta());
			ps.setString(7, datiUtenteModificato.getSesso());
			ps.setString(8, datiUtenteModificato.getCitta());
			ps.setString(9, datiUtenteModificato.getRole());
			// Esecuzione query
			ps.executeUpdate();
			// Restituisce true se � andato a buon fine
			return true;
			// Eccezioni nel caso qualcosa non sia andato a buon fine
		} catch (Exception e) {
			e.printStackTrace();
			// E restituzione di false
			return false;
			// Chiude le connessioni con il db
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @param idUtenteDaEliminare
	 *            id dell'utente da eliminare
	 * @return true se l'eliminazione � andata a buon fine, false altrimenti
	 * @throws SQLException
	 *             eccezione sql
	 * @throws ClassNotFoundException
	 *             eccezione
	 * 
	 *             Questo metodo elimina un utente dato il suo id
	 */
	public boolean eliminaUtente(int idUtenteDaEliminare) throws SQLException,
			ClassNotFoundException {
		PreparedStatement ps = null;
		String sql = "";
		Connection conn = null;
		try {
			conn = getConnection();
			// Query che elimina l'utente dal db
			sql = ("DELETE FROM user WHERE idUser=?");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, idUtenteDaEliminare);
			// Esegue la query
			ps.executeUpdate();
			// nel caso in cui sia andato a buon fine, restituisce true
			return true;
			// Lancia eccezioni nel caso ci siano errori
		} catch (Exception e) {
			e.printStackTrace();
			// E restituisce false
			return false;
			// Chiude le connessioni con il db
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

	/**
	 * 
	 * @return elenco di tutti gli utenti presente nel db
	 * @throws ClassNotFoundException
	 *             eccezione
	 * @throws SQLException
	 *             eccezione sql
	 * 
	 *             Questo metodo restituisce una lista di tutti gli utenti
	 */
	public List<User> elencoUtenti() throws ClassNotFoundException,
			SQLException {
		PreparedStatement ps = null;
		String sql = "";
		// Lista in cui verranno inseriti gli utenti
		ArrayList<User> users = new ArrayList<User>();
		Connection conn = null;
		try {
			conn = getConnection();
			// Query
			sql = ("SELECT * FROM user WHERE role!='platform admin'");
			ps = conn.prepareStatement(sql);
			// Esegue la query
			ResultSet rs = ps.executeQuery();
			// Per ogni resul set inserisce un nuovo utente nella lista
			while (rs.next())
				users.add(new User(rs.getInt("idUser"), rs.getString("name"),
						rs.getString("surname"), rs.getString("email"), rs
								.getString("username"), rs
								.getString("password"), rs.getInt("eta"), rs
								.getString("sesso"), rs.getString("citta"), rs
								.getString("role")));
			// Restituisce la lista degli utenti
			return users;
			// Lancia l'eccezione nel caso ci siano errori
		} catch (Exception e) {
			e.printStackTrace();
			// E restitisce una lista vuota
			return null;
			// Chiude le connessioni
		} finally {
			if (conn != null)
				conn.close();
			if (ps != null)
				ps.close();
		}
	}

}
