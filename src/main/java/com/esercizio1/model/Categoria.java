package com.esercizio1.model;

/**
 * Questa classe modella la categoria merceologica
 * 
 * @author Gruppo Silvi
 *
 */
public class Categoria {

	private int idCategoria;
	private String name;

	public Categoria(int idCategoria, String name) {
		this.idCategoria = idCategoria;
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	public Categoria() {
		super();
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
