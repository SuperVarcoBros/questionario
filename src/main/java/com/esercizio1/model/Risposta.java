package com.esercizio1.model;

/**
 * Questa classe modella le risposte date dall'utente ai sondaggi
 * 
 * @author Gruppo Silvi
 *
 */
public class Risposta {

	private int fkIdUser;
	private int fkIdSondaggio;
	private int fkIdDomanda;
	private String risposta;
	
	
	public int getFkIdUser() {
		return fkIdUser;
	}

	public void setFkIdUser(int fkIdUser) {
		this.fkIdUser = fkIdUser;
	}

	public int getFkIdSondaggio() {
		return fkIdSondaggio;
	}

	public void setFkIdSondaggio(int fkIdSondaggio) {
		this.fkIdSondaggio = fkIdSondaggio;
	}

	public int getFkIdDomanda() {
		return fkIdDomanda;
	}

	public void setFkIdDomanda(int fkIdDomanda) {
		this.fkIdDomanda = fkIdDomanda;
	}

	public String getRisposta() {
		return risposta;
	}

	public void setRisposta(String risposta) {
		this.risposta = risposta;
	}

}
