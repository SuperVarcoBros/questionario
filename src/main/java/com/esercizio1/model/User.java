package com.esercizio1.model;

import java.util.List;

/**
 * Questa classe modella gli utenti
 * 
 * @author Gruppo Silvi
 *
 */
public class User {

	private int idUser;
	private String name;
	private String surname;
	private String email;
	private String username;
	private String password;
	private int eta;
	private String sesso;
	private String citta;
	private String role;
	private List<Categoria> preferenze;

	public User(int idUser, String name, String surname, String email,
			String username, String password, int eta, String sesso,
			String citta, String role) {
		super();
		this.idUser = idUser;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.username = username;
		this.password = password;
		this.eta = eta;
		this.sesso = sesso;
		this.citta = citta;
		this.role = role;
	}

	public User() {
		super();
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String toString() {
		return "Name: " + getName() + " Surname:  " + getSurname()
				+ " Email:      " + getEmail() + " Role:      " + getRole()
				+ "Password:      " + getPassword();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}

	public String getSesso() {
		return sesso;
	}

	public void setSesso(String sesso) /* throws Exception */{
		/*
		 * if(sesso.equals("M") || sesso.equals("m") || sesso.equals("F") ||
		 * sesso.equals("f")) throw new Exception("Sesso errato"); else
		 */
		this.sesso = sesso;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public List<Categoria> getPreferenze() {
		return preferenze;
	}

	public void setPreferenze(List<Categoria> preferenze) {
		this.preferenze = preferenze;
	}

}