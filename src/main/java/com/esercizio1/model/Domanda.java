package com.esercizio1.model;

/**
 * Questa classe modella le domande presenti nei sondaggio
 * 
 * @author Gruppo Silvi
 *
 */
public abstract class Domanda {

	private int idDomanda;
	private String domanda;
	private String risposte[] = new String[4];
	//Id del creatore della domanda
	private int fkIdUser;
	
	public Domanda(int idDomanda, String domanda, String[] risposte, int fkIdUser) {
		super();
		this.idDomanda = idDomanda;
		this.domanda = domanda;
		this.risposte = risposte;
		this.fkIdUser = fkIdUser;
	}
	
	public Domanda() {
		super();
	}
	
	public int getIdDomanda() {
		return idDomanda;
	}
	public void setIdDomanda(int idDomanda) {
		this.idDomanda = idDomanda;
	}
	public String getDomanda() {
		return domanda;
	}
	public void setDomanda(String domanda) {
		this.domanda = domanda;
	}
	public String[] getRisposte() {
		return risposte;
	}
	public void setRisposte(String[] risposte) {
		this.risposte = risposte;
	}
	
	public int getFkIdUser() {
		return fkIdUser;
	}
	public void setFkIdUser(int fkIdUser) {
		this.fkIdUser = fkIdUser;
	}
	
	

}
