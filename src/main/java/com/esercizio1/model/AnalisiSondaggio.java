package com.esercizio1.model;

/**
 * Questa classe modella l'analisi dei sondaggi
 * 
 * @author Gruppo Silvi
 *
 */
public class AnalisiSondaggio {
	
	private int idSondaggio;
	private int idDomanda;
	private String risposta;
	private int occorrenzaRisposta;
	
	public AnalisiSondaggio(int idSondaggio, int idDomanda, String risposta, int occorrenzaRisposta) {
		this.idDomanda = idDomanda;
		this.idSondaggio = idSondaggio;
		this.risposta = risposta;
		this.occorrenzaRisposta = occorrenzaRisposta;
	}
	
	public AnalisiSondaggio() {
		super();
	}
	
	public int getIdSondaggio() {
		return idSondaggio;
	}
	public void setIdSondaggio(int idSondaggio) {
		this.idSondaggio = idSondaggio;
	}
	public int getIdDomanda() {
		return idDomanda;
	}
	public void setIdDomanda(int idDomanda) {
		this.idDomanda = idDomanda;
	}
	public String getRisposta() {
		return risposta;
	}
	public void setRisposta(String risposta) {
		this.risposta = risposta;
	}
	public int getOccorrenzaRisposta() {
		return occorrenzaRisposta;
	}
	public void setOccorrenzaRisposta(int occorrenzaRisposta) {
		this.occorrenzaRisposta = occorrenzaRisposta;
	}
	
	

}
