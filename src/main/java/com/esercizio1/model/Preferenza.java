package com.esercizio1.model;

/**
 * Questa classe modella le preferenze sulle categorie merceologiche dell'utente
 * 
 * @author Gruppo Silvi
 *
 */
public class Preferenza {
	private Categoria categoria;
	private Boolean value;

	@Override
	public String toString() {
		return "Preferenza [categoria=" + categoria + ", value=" + value + "]";
	}

	public Preferenza() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Preferenza(Categoria categoria, Boolean value) {
		super();
		this.categoria = categoria;
		this.value = value;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

}
