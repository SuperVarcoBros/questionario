package com.esercizio1.model;

import java.util.Date;

/**
 * questa classe modella i sondaggi
 * 
 * @author Gruppo Silvi
 *
 */
public class Sondaggio {

	private int idSondaggio;
	private int fkIdUser;
	private Date dataCreazione;
	private Date dataScadenza;
	private Categoria categoria;
	private Domanda[] domande;

	public Sondaggio() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Sondaggio(int idSondaggio, int fkIdUser, Date dataCreazione,
			Date dataScadenza, Categoria categoria) {
		super();
		this.idSondaggio = idSondaggio;
		this.fkIdUser = fkIdUser;
		this.dataCreazione = dataCreazione;
		this.dataScadenza = dataScadenza;
		this.categoria = categoria;
	}

	public int getIdSondaggio() {
		return idSondaggio;
	}

	public void setIdSondaggio(int idSondaggio) {
		this.idSondaggio = idSondaggio;
	}

	public Date getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Date getDataScadenza() {
		return dataScadenza;
	}

	public void setDataScadenza(Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	public Boolean isComplete() {
		if (this.dataScadenza.after(new Date()))
			return false;
		else
			return true;
	}

	public Domanda[] getDomande() {
		return domande;
	}

	public void setDomande(Domanda[] domande) {
		this.domande = domande;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public int getFkIdUser() {
		return fkIdUser;
	}

	public void setFkIdUser(int fkIdUser) {
		this.fkIdUser = fkIdUser;
	}

}
