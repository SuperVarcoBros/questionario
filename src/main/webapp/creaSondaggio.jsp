<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Crea nuovo sondaggio</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.min.css" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js"></script>
<title>Login page</title>
<style type="text/css">
.label {
	color: #000;
}
</style>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="homeAdmin.jsp">SurveyPlat</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-expanded="false">Menu
							<span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<s:url action='creaSondaggioInput'/>">Crea
									Sondaggio</a></li>
							<li><a href="<s:url action='visualizzaSondaggi'/>">Modifica/Elimina
									Sondaggi</a></li>
							<li><a href="<s:url action='AnalisiSondaggi'/>">Visualizza
									Analisi Sondaggi</a></li>
						</ul></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><p class="navbar-text">
							Signed in as <b><s:property value="userBean.username" /></b>
						</p></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li><a href="<s:url action='logOut'/>">Logout</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>


	<div class="container">
		<!-- <div class="row"> -->
		<h3>Crea Domanda</h3>
		<s:form action="creaDomanda">
			<s:submit align="left" value="Crea Nuova Domanda"
				cssClass="btn btn-default" />
		</s:form>
		<h3>Crea Sondaggio</h3>
		<s:form action="creaSondaggio" cssClass="form-horizontal"
			theme="simple">
			<div class="row">
				<div class="col-md-5">
					<div class="hero-unit">

						<s:textfield label="Data Scadenza" cssClass="form-control"
							theme="xhtml" name="dataScadenza" id="dataScadenza"></s:textfield>
						<script type="text/javascript">
							$(document).ready(function() {
								$('#dataScadenza').datepicker({
									format : "dd/mm/yyyy"
								});
							});
						</script>
					</div>
				</div>
				<div class="col-md-5 col-md-offset-2">
					<div class="form-group">
						<s:select label="Seleziona la categoria" list="listaCategorie"
							name="nomeCategoria" cssClass="form-control" theme="xhtml" />
					</div>
				</div>
			</div>
			<br />
			<p>Lista domande disponibili:</p>
			<br />
			<table class="table table-striped table-responsive">
				<tr>
					<th>id Domanda</th>
					<th>id User</th>
					<th>corpo</th>
					<th>ris1</th>
					<th>ris2</th>
					<th>ris3</th>
					<th>ris4</th>
					<th>sel</th>
				</tr>
				<s:iterator value="listaDomande">
					<tr>
						<td><s:property value="idDomanda" /></td>
						<td><s:property value="fkIdUser" /></td>
						<td><s:property value="domanda" /></td>
						<s:iterator value="risposte" var="risposta">
							<td><s:property value="risposta" /></td>
						</s:iterator>
						<td><s:checkbox name="checkboxes[%{idDomanda}]"
								cssClass="checkbox-inline" theme="simple" /></td>
					</tr>
				</s:iterator>
			</table>

			<s:submit align="right" cssClass="btn btn-success" />
		</s:form>

	</div>
	<footer class="footer">
		<hr />
		<div class="container">
			<center>
				<p>fadnlsfdldsflndsflsdf�fdssfsdjfsdknfsdjnfkjsdfjksdfkjsdf</p>
				<p>fadnlsfdldsflndsflsdf�fdssfsdjfsdknfsdjnfkjsdfjksdfkjsdf</p>
			</center>
		</div>
	</footer>
</body>
</html>