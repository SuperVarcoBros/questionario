<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<title>Register</title>
<style type="text/css">
.label {
	color: #000;
}
</style>
<s:head />
</head>
<body>
	<s:if test="userBean.role=='platform admin'">

		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="homePlatformAdmin.jsp">SurveyPlat</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-expanded="false">Menu
								<span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<s:url action='registerInput'/>">Crea
										Utente/Admin</a></li>
								<li><a href="<s:url action='elencoUtenti'/>">Modifica/Elimina
										User</a></li>
							</ul></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><p class="navbar-text">
								Signed in as <b><s:property value="userBean.username" /></b>
							</p></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="<s:url action='logOut'/>">Logout</a></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
	</s:if>
	<s:else>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

					<a class="navbar-brand" href="<s:url action='loginInput'/>">SurveyPlat</a>


				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">About <span
								class="sr-only">(current)</span></a></li>
						<li><a href="#">Contact</a></li>
					</ul>

					<ul class="nav navbar-nav navbar-right">

					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>

	</s:else>





	<div class="container">
		<div class="row">
			<div class="col-md-3 col-md-offset-4">
				<s:if test="userBean.role=='platform admin'">
					<h3>Crea utente/admin</h3>
				</s:if>
				<s:else>
					<h3>Register for a prize by completing this form</h3>
				</s:else>

				<s:form action="register">
					<s:textfield name="userBean.name" value="" label="name"
						cssClass="form-control" labelposition="left" />
					<s:textfield name="userBean.surname" value="" label="surname"
						cssClass="form-control" />
					<s:textfield name="userBean.email" value="" label="email"
						cssClass="form-control" />
					<s:textfield name="userBean.username" value="" label="username"
						cssClass="form-control" />
					<s:password name="userBean.password" value="" label="password"
						cssClass="form-control" />
					<s:textfield name="userBean.eta" value="" label="age"
						cssClass="form-control" />
					<s:radio name="userBean.sesso" list="#{'M':'M','F':'F'}"
						label="sex" cssClass="radio-inline" />
					<s:textfield name="userBean.citta" value="" label="city"
						cssClass="form-control" />
					<s:if test="userBean.role=='platform admin'">
						<s:combobox label="select role"
							list="#{'user':'user', 'admin':'admin'}" name="userBean.role"
							value="---Select---" cssClass="form-control" />
					</s:if>
					<s:else>
						<s:hidden key="userBean.role" value="user" />
					</s:else>
					<s:submit cssClass="btn btn-primary" />

				</s:form>

				<br />
				<s:if test="userBean.role=='platform admin'">
					<a href="homePlatformAdmin.jsp"><button>Home</button></a>
				</s:if>
				<s:else>
					<%-- <a href="<s:url action='loginInput'/>" class="btn btn-default"><button>Login
							page</button></a> --%>
				</s:else>
			</div>
		</div>
	</div>
	<footer class="footer">
		<hr />
		<div class="container">
			<center>
				<p>fadnlsfdldsflndsflsdf�fdssfsdjfsdknfsdjnfkjsdfjksdfkjsdf</p>
				<p>fadnlsfdldsflndsflsdf�fdssfsdjfsdknfsdjnfkjsdfjksdfkjsdf</p>
			</center>
		</div>
	</footer>

</body>
</html>