<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.min.css" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js"></script>
<title>Login page</title>
<style type="text/css">
.label {
	color: #000;
}
</style>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="homeAdmin.jsp">SurveyPlat</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-expanded="false">Menu
							<span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<s:url action='creaSondaggioInput'/>">Crea
									Sondaggio</a></li>
							<li><a href="<s:url action='visualizzaSondaggi'/>">Modifica/Elimina
									Sondaggi</a></li>
							<li><a href="<s:url action='AnalisiSondaggi'/>">Visualizza
									Analisi Sondaggi</a></li>
						</ul></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><p class="navbar-text">
							Signed in as <b><s:property value="userBean.username" /></b>
						</p></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li><a href="<s:url action='logOut'/>">Logout</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>


	<div class="container">
		<h3>Sondaggi:</h3>
		<table class="table table-striped table-responsive">
			<tr>
				<th>Id Sondaggio</th>
				<th>Id User</th>
				<th>Data Creazione</th>
				<th>Data Scadenza</th>
				<th>Categoria</th>
				<th>Cancella</th>
			</tr>
			<s:iterator value="visualizzaSondaggi">
				<tr>
					<td><s:property value="idSondaggio" /></td>
					<td><s:property value="fkIdUser" /></td>
					<td><s:property value="dataCreazione" /></td>
					<td><s:property value="dataScadenza" /></td>
					<td><s:property value="categoria.name" /></td>
					<td><s:form action="deleteSondaggio"
							cssClass="form-horizontal" theme="simple">
							<s:hidden name="idSondaggio" value="%{idSondaggio}" />
							<s:submit value="X" cssClass="btn btn-success" />
						</s:form></td>
				</tr>
			</s:iterator>
		</table>
	</div>


	<footer class="footer">
		<hr />
		<div class="container">
			<center>
				<p>fadnlsfdldsflndsflsdf�fdssfsdjfsdknfsdjnfkjsdfjksdfkjsdf</p>
				<p>fadnlsfdldsflndsflsdf�fdssfsdjfsdknfsdjnfkjsdfjksdfkjsdf</p>
			</center>
		</div>
	</footer>


</body>
</html>